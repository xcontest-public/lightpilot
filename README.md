# Lightpilot theme

Mapsforge/VTM rendering theme for pilots.

The main goal of this theme is de-cluttering the map screen and making the XCTrack flight 
information (airspaces, FAI assistant etc.) more visible.

- Uses light colors that do not conflict with other colors on XCTrack (light, bluish)
- Hides unimportant objects (fields, meadows etc.)
- Displays city names only on higher zoom levels

Best suited for [OpenAndroMaps](https://www.openandromaps.org/en/downloads/countrys-and-regions) offline maps and [XCTrack](https://xctrack.org/), but can be used with any software supporting VTM themes.


License: MIT

Based on hyperpilot theme

